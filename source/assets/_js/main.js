// Gumby is ready to go
Gumby.ready(function() {
	console.log('Gumby is ready to go...', Gumby.debug());

	// placeholder polyfil
	if(Gumby.isOldie || Gumby.$dom.find('html').hasClass('ie9')) {
		$('input, textarea').placeholder();
	}
});

// Oldie document loaded
Gumby.oldie(function() {
	console.log("This is an oldie browser...");
});

// Touch devices loaded
Gumby.touch(function() {
	console.log("This is a touch enabled device...");
});

// Document ready
jQuery(document).ready(function() {
    $('.headerSlider').bxSlider({
        mode: 'fade',
        speed: 1000,
        auto: true,
        pager: true,
        randomStart: true,
        controls: false,
        adaptiveHeight: true
    });
    $('#indexSlider ul').bxSlider({
        mode: 'fade',
        speed: 1000,
        auto: true,
        pager: false,
        controls: false
    });   
    $(".gallery").colorbox({
        rel:'gallery',
        opacity: 0.8,
        maxWidth: '90%',
        maxHeight: '90%',
        scrolling: false
    });
    $(".imageBox").colorbox({
        opacity: 0.8,
        maxWidth: '90%',
        maxHeight: '90%',
        scrolling: false
    });
});

// responsive colorbox
$(window).resize(function() {
    if ($('#cboxOverlay').is(':visible')) {
        $.colorbox.load(true);
    }
});