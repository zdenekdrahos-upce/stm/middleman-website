
set :webPrefix, ''
configure :build do
    set :webPrefix, '/stm-sport'
end


require 'tzinfo'
activate :blog do |blog|
  blog.prefix = "blog"
  blog.permalink = "{title}-{year}-{month}-{day}.html"
  blog.sources = "{year}-{month}-{day}-{title}.html"
  blog.default_extension = ".md"
end

page "*", :layout => :page
page "*/*", :layout => :page
page "blog/*", :layout => :blog

set :css_dir, 'assets'
set :js_dir, 'assets'
set :images_dir, 'assets/images'

configure :build do
  activate :minify_css
  activate :minify_html, remove_http_protocol: false, remove_https_protocol: false
  activate :minify_javascript
  # activate :asset_hash # doesn't work with hardcoded webPrefix
  activate :relative_assets
end
